print("Ejemplo de keywords arguments 1")

""" 
    Ejemplo para pasar keyword arguments, por ejemplo, un diccionario a una función pasando su clave y valor. 
"""

def named(**kwards):
    print(kwards)
detail = {"name": "Bob", "age": 25}
named(**detail)

""" 
    Ejemplo2 para pasar keyword arguments, por ejemplo, un diccionario a una función pasando su clave y valor. 
    En este ejemplo se hace una llamada a otra función con kwards y se reformatea la salida. 
"""
def print_nicely(**kwards):
    named(**kwards)
    for name, age in kwards.items():
        print(f"{name}: {age}")
print_nicely(name="Perico", age=41)
""" 
    Se puede utilizar ambos, 'posicional arggument' y 'keywords arguments' en una misma función de la siguiente forma.
"""
def both (*args,**kwards):
    print(args)
    print(kwards)
    for item in kwards.items():
        print(f"elemento del diccionario: {item}")
both(1,2,3,4,asignatura1="Ciencia", asignatura2="Literatura", creditos=23)

