""" 
    Existen métodos que son llamados por python por defecto, uno de ellos esl __init__
"""
class Person:
    def __init__(self,name,age): #método mágico
        self.name = name
        self.age = age 
    def __str__(self): #método mágico
        return f"Person {self.name} tiene {self.age}"

    def __repr__(self): #método mágico para depurar. 
        return f"<Person ({self.name} tiene {self.age})>"
    


employ = Person("Bob", 21)

"""
    La salida del anterior comando monstrará un string que representa el objeto employ que ha creado :

        <__main__.Person object at 0x000001FA8C41C240>
    En el caso de que queramos modificar por ejemplo el anterior objeto , se puede utilizar la anterior string y 
    convertir un objeto en una cadena se utilizaría el método mágico __str__ 
    
"""
""" 
    Para el ejemplo de _str__ , la llamada de print al objeto devuelve la cadena que tenga definida en el método
    ___def__str__ 
"""
print(employ)
""" 
    Para poder utilizar el método de depuración __repr__ con print se debe comentar el bloque de __str__ ya que por defecto
    python asumen que debe tomar este úlitmo como salida.  
"""
    