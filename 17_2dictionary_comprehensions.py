""" 
    Ejemplo que genera promedio de notas.
 """
student = {
    'name': 'José',
    'school':'Computing',
    'grades': (66,77,88)
     
}
student_l = [
    {
    'name': 'José',
    'school':'Computing',
    'grades': (66,77,88)
    },
    
    {'name': 'Perico',
    'school':'Computing',
    'grades': (66,99,88)
    } 
]


def average_grade(data):
    grades = data['grades']
    return sum(grades)/len(grades)

def average_grade_all_students(student_list):
    total = 0
    count = 0
    for student in student_list:
        total += sum(student['grades'])
        count += len(student['grades'])
    return total / count

print(average_grade(student))
print(average_grade_all_students(student_l))