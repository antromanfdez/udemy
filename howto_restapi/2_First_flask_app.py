#  https://github.com/schoolofcode-me/rest-api-sections/tree/master/section3

# las aplicaciones flask están basadas en request y response

# request: 
#     Cuando solicitas la info de una página. 

from flask import Flask #las clases siempre empieza con mayúsculas que es lo que vamos a importar

app = Flask(__name__) #creamos la clase con un nombre único 
#que tipo de request debe entender

@app.route('/') #viene a indicar la página--> ej 'http://www.google.es/', @app.route('/plus') --> 'http://www.google.es/plus' etc

def home():
    return "Hello world!" #cuando accedamos a nuestro navegador lo que veremos es el return "Hello world!"

app.run(port=5000) #para decirle a nuestra apliación que se incie con un puerto específico. si indica que ya existe cmabiar por otro 

#Si abrimos la dirección  http://127.0.0.1:5000/ en un navegador recibiremos la salida "Hello world!"
