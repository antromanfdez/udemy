#Simular una tienda online,
#NOTA: Los endpoints se crean con ej. @app.route('/')
# RECORDATORIO: Por defecto los navageadores devuelven GET
from flask import Flask, jsonify, request, render_template

app = Flask(__name__) 
stores = [
    #aunque json tiene la estructura descrita del siguiente diccionario , es necesario hacer una conversión para hacer que este dato
    # se convierta de un diccionario en python a un dato json. para ello se utiliza jsonfly
    # JSON SIEMPRE UTILIZA DOBLE COMILLAS Y NO SIMPLES
    {
        'name': "My Wonderful Store",
        'items': [
            {
            'name': 'My Item', 
            'price': 15.99
            }
        ]
    }
]

"""
Como estamos mirando desde el lado servidor, cambiamos el sentido de los verbos POST y GET.
POST -- used to receive data
GET  -- used to send data back only 

"""
@app.route('/')
#Este endpont renderiza nuestra api desde javascript index.html. 
def home():
    return render_template('index.html')
#POST /store data: {name:}
@app.route('/store', methods=['POST']) 
def create_store():
    #crea un nuevo store 
    request_data = request.get_json()
    new_store = {
        'name': request_data['name'],
        'items':  []
        
    }
    stores.append(new_store)
    return jsonify(new_store)
    # return jsonify({'stores': stores}) # devolviendo el store completo


@app.route('/store/<string:name>')  # 'http://127.0.0.1:5000/store/some_name'
def get_store(name):
    #Iterate over stores
    #If the store name matches, return it
    #If none match, return an error message

    for store in stores:
        if store['name'] == name:
            return jsonify(store)
    
    return jsonify({'message': 'store not found'})

@app.route('/store')  
def get_stores():
     #return jsonfly(stores) #conviert la variable stores en json y ya que la variable es una lista la tenemos que meter en un dicionario de 
    #  la siguiente forma
    return jsonify({'stores': stores})
    
# POST /store/<string:name>/item {name:, price:}
@app.route('/store/<string:name>/item', methods=['POST']) 
def create_item_in_store(name):
    request_data = request.get_json()
    for store in stores: 
        if store['name'] == name:
            new_item = {
                'name': request_data['name'],
                'price': request_data['price']

            }
            store['items'].append(new_item)
            # return jsonify(new_item) #muestra el nuevo item añadido
            return jsonify(store) #muestra store con el nuevo item añadido
    return jsonify({'message': 'store not found'})


# GET /store/<string:name>/item
@app.route('/store/<string:name>/item') 
def get_item_in_store(name):
    for store in stores:
        if store['name'] == name:
            return jsonify({'items': store['items']})
    return jsonify({'message': 'store not found'})

app.run(port=5000) 