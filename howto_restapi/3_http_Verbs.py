"""
http es uno de los protocolos más conocidos. 


Que es un web server. 
    - Es hardware
    - Es una pieza de código para aceptar incoming web requests

Cuando se accede a http://wwww.google.es.com lo que recibimos es es una 'get request'

    get / HTTP/1.1
    Host: www.google.es

GET
    Se puede recibir error también 
        http no sporotado
    
    Para el ejemplo de la página de login de twitter 'https://twitter.com/login':
        get /login http/1.1
        Host: https://twitter.com

Por defecto los navegadores hacen GET de las páginas aunque has más cosas que se pueden realizar:

    POST, DELETE, PUT, OPTIONS, HEAD...


HTTP Verbs
    GET         Retrieve somethings     ---> GET /item/1
    POST        Receive data, and use it --> POST /item 
    PUT         make sure something is there --> PUT/item #Si envío un json se asegura que esa información esté. Modifica un determinado dato. 
    DELELTE     Remove somethings       --> DELETE /item/1 

"""