#Simular una tienda online,
#NOTA: Los endpoints se crean con ej. @app.route('/')
# RECORDATORIO: Por defecto los navageadores devuelven GET
from flask import Flask, jsonify

app = Flask(__name__) 
stores = [
    #aunque json tiene la estructura descrita del siguiente diccionario , es necesario hacer una conversión para hacer que este dato
    # se convierta de un diccionario en python a un dato json. para ello se utiliza jsonfly
    #JSON SIEMPRE UTILIZA DOBLE COMILLAS Y NO SIMPLES
    {
        'name': "My Wonderful Store",
        'items': [
            {
            'name': 'My Item', 
            'price': 15.99
            }
        ]
    }
]

"""
Como estamos mirando desde el lado servidor, cambiamos el sentido de los verbos POST y GET.
POST -- used to receive data
GET  -- used to send data back only 

"""
#POST /store data: {name:}
@app.route('/store', methods=['POST']) 
def create_store():
    pass

# GET  /store/<string:name>
@app.route('/store/<string:name>')  # 'http://127.0.0.1:5000/store/some_name'
def get_store(name):
    pass

# GET /store
@app.route('/store')  
def get_stores():
     #return jsonfly(stores) #conviert la variable stores en json y ya que la variable es una lista la tenemos que meter en un dicionario de 
    #  la siguiente forma
    return jsonify({'stores': stores})

# POST /store/<string:name>/item {name:, price:}
@app.route('/store/<string:name>', methods=['POST']) 
def create_item_store(name):
    pass

# GET /store/<string:name>/item
@app.route('/store/<string:name>/item') 
def get_item_store(name):
    pass

app.run(port=5000) 
