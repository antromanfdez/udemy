from flask import Flask, request
from flask.json import jsonify
from flask_restful import Resource, Api, reqparse 
from flask_jwt import JWT, jwt_required 
# Flask-JWT srive para controlar la autenticación. pip istall Flask-JWT y se mostrará como PyJWT
# también se importa el decorator @jwt_requiered de tal manera que es necesario autenticar cuando se
# antepone a la llamada a algún método.
# el módulo reqparse sirve para en la request modificar solo una parte del payload(dato)
from security import authenticate, identity
# Resource , se utilizar por ejemplo en el caso de artículos, pianos , será un resoure
# Los resource son normalmente mapeados a BB.DD. por lo que es algo a tener en cuenta.
app = Flask(__name__)
app.secret_key = 'password'
api = Api(app) #Permite de una manera muy fácil añadir resources a nuestra app. 
jwt = JWT(app,authenticate, identity) 
# JWT crea un nuevo endpoint > /auth, cuando llamamos a auth se envia un user y password
# el endpoint /auth de postman se ha creado un header tipo content-type con value application/json y en 
# body se introducido coomo valor
# raw:  
    # {
    # "username": "bob",
    # "password": "asdf"
    # }
items = []
# IMPORTANTE: CON FLASK RESTFULL YA NO ES NECESARIO UTILIZAR JSONIFY PARA DEVOLVER EL JSON YA QUE DEVUELVE 
# EL DICCIONARIO PYTHON POR SI MISMO



#Todos los resource debenn ser clases
class Item(Resource):
 ##Inicio de Código de clase reutilizable por otros metodos de la clase (post y put)#####
    parser = reqparse.RequestParser() #habilitado con el modulo request para modificar tan solo una parte del payload 
    # y al cual se le puede indicar ciertos parámetros adicionales (que sea float , info, con valor ..)
    # TAMBIÉN le estamos indicando que el el único argumente esperado es 'price'
    
    parser.add_argument('price',
        type=float,
        required=True,
        help="This field cannot be left blank!"            
    )
##Fin de Código de clase reutilizable por otros metodos de la clase (post y put)#####
    @jwt_required() # con este requiered , el siguiente método debe contener en el header de postman 
    #una key de tipo 'Authorization'con valor JWT seguida de la cadena access_token devuelta por /auth previamente.
    def get(self, name): #con el nombre del estudiante
        # for item in items:
        #     if item['name'] == name:
        #         return item
        item = next(filter(lambda x: x['name'] == name, items), None) 
        #la anterior linea sustituye al bucle for: busca mediante next, filter y lambda, la primera coincidencia. 
        # Con none evitamos  una salida de error en el caso de que la lista se ecuentre vacía.

        return {'item': item}, 200 if item else 404 # El error que debe devolver cuando no encuentra algo es 404

    def post(self, name):
        #item = {'name': name, 'price': 12.00}
        if next(filter(lambda x: x['name'] == name, items), None): # si no es None 
            return {'message': "An item with name '{}' already exists'".format(name)}, 400 #Este código es para un bad request
         
        #importante importar request from flask para que funcione la request.get_json: con 'force=True' le indicamos que no necesitamos el context-type
        #y que lo formatearemos incluso aunque no tenga application/json
        #con 'silent=True' no da ningún error
            # data = request.get_json(force=True) 
            # data = request.get_json(silent=True) 
        data = Item.parser.parse_args() # hace una llamada al código definido al principio de la clase
        item = {
            'name': name,
            'price': data['price']
        }
        items.append(item)
        return item, 201 #El código a devolver cuando algo se crea correctamente e 201
    def delete (self,name):
        global items
        items = list(filter(lambda x: x['name'] != name, items))
        #ya que se está dirigiendo a una misma variable con 'global items' le inidicamos que modifique la lista global items
        return {'message': 'item deleted'}
    
    # def put(self, name):  #crea o actualiza un determinado item. No importa cuantas veces se llame que siempre actualizará el mismo item.
    #     data = request.get_json()
    #     item = next(filter(lambda x: x['name'] == name, items), None)
    #     if item is None:
    #         item = {'name': name, 'price': data['price']}
    #         items.append(item)
    #     else:
    #         item.update(data)   #si el item existe las lista tienen la propiedad update, por lo que se podría actualizar los datos
    #         #obtenidos en item con la función lambda        
    #     return item 

    ##### El sigiuiente put modificar con requparse una determinada parte del payload #############
    def put(self, name):  #crea o actualiza un determinado item. No importa cuantas veces se llame que siempre actualizará el mismo item.
        data = Item.parser.parse_args() # hace una llamada al código definido al principio de la clase
        item = next(filter(lambda x: x['name'] == name, items), None)
        if item is None:
            item = {'name': name, 'price': data['price']}
            items.append(item)
        else:
            item.update(data)   #si el item existe las lista tienen la propiedad update, por lo que se podría actualizar los datos
            #obtenidos en item con la función lambda        
        return item 


        

class ItemList(Resource):
    def get(self):
        return {'items': items}

api.add_resource(Item, '/item/<string:name>') #el modo de acceder a los endpoing similar a los decoradores @app.route('/store')
api.add_resource(ItemList, '/items')
app.run(port=5000, debug=True) #con el parámetro debug podemos depurar los errores mostrados.

