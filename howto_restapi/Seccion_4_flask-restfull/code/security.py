from user import User
# users = [
#     {
#         'id': 1,
#         'username': 'bob',
#         'password': 'asdf'

#     }

# ]
# username_mapping = {'bob': {
#         'id': 1,
#         'username': 'bob',
#         'password': 'asdf'

#     }
# }
# #username_mapping['bob']
# userid_mapping = {1: {
#         'id': 1,
#         'username': 'bob',
#         'password': 'asdf'
#     }
# }
#userid_mapping=[1]

# LAS ANTERIRORES LISTA DE DICCIONARIOS , AL UTILIZAR LA CLASE CREADA EN EL FICHERO USER.PY QUEDARÍA DE LA SIGUIENTE MANERA. 
users = [
    User(1, 'bob', 'asdf')

]
username_mapping = {u.username: u for u in users}
userid_mapping = {u.id: u for u in users}

def authenticate(username, password):
    user = username_mapping.get(username, None) #get es otra manera de acceder al valor de una key
    if user and user.password == password: 
        return user

def identity(payload): # es el content de PyJWT 
    user_id = payload['identity']
    return userid_mapping.get(user_id,None)