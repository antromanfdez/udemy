import sqlite3 # 
connection = sqlite3.connect('data.db') #Todos los datos son almacenados en una fichero 'data.db'

cursor = connection.cursor() #permite seleccionar cosas y alamacenarlas (querys)
create_table = "CREATE TABLE users (id int, username text, password text)"
cursor.execute(create_table)
user = (1, 'jose', 'asdf') # crear el primer usuario
insert_query = "INSERT INTO users VALUES (?,?,?)"
cursor.execute(insert_query, user) #cursos se encarga de sutituir los ? por los valores posicionales creados en la tabla user
users = [
    (2, 'rolf', 'asdf'),
    (3, 'anne', 'xyz'),
    
]
#para añadir consulta muliple se utliza 'cursos.executemany'
cursor.executemany(insert_query, users)
select_query = "select * from users"
for row in cursor.execute(select_query):
    print(row)
connection.commit() #para reflejar los cambios anteriores al fichero data.db
connection.close() #cierra de la database
