import sqlite3
from flask_restful import Resource,reqparse
class User():
    
    def __init__(self, _id, username, password):
        self.id = _id # se utiliza '_id' ya que 'id' es una palabra reservada de python.
        self.username = username
        self.password = password
    #dar la opción a la de clase de de obtener los datos a a través de sqlite a diferencia de la Sección4
    @classmethod
    def find_by_username(cls, username):
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        query = "SELECT * FROM users WHERE username=?"
        result = cursor.execute(query, (username,)) #el parámetro tiene que ser en la forma de una tabla como (username,)
        row = result.fetchone() #obtendrá la primera coincidencia de result
        if row:
            #user = cls(row[0], row[1], row[2])
            user = cls(*row) #simplificacion de línea anterior
        else:
            user = None

        connection.close()
        return user

#dar la opción a la de clase de de obtener los datos a a través de sqlite a diferencia de la Sección4
    @classmethod
    def find_by_id(cls, _id):
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        query = "SELECT * FROM users WHERE id=?"
        result = cursor.execute(query, (_id,)) #el parámetro tiene que ser en la forma de una tabla como (_id,)
        row = result.fetchone() #obtendrá la primera coincidencia de result
        if row:
            #user = cls(row[0], row[1], row[2])
            user = cls(*row) #simplificacion de línea anterior
        else:
            user = None

        connection.close()
        return user

class UserRegister(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('username', 
        type=str,
        required=True, 
        help="This filed cannot be blank"
    )
    parser.add_argument('password', 
        type=str,
        required=True, 
        help="This filed cannot be blank"
    )
    
    

    def post(self):
        data = UserRegister.parser.parse_args()  #obtenemos los datos del jason payload
        if User.find_by_username(data['username']): #Informa con valor 400 que ya existe un usuario con ese nombre
            return {"message": "A user with that username already exists"}, 400 
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        query = "INSERT INTO users VALUES (NULL,?,?)" #NULL ya que es un valor incrementado automáticamente
        cursor.execute(query, (data['username'], data['password'])) # se tienen que pasar los data de username y password entre () ya ques una table
        connection.commit()
        connection.close()
        return {"message" : "User created successfully"}, 201



