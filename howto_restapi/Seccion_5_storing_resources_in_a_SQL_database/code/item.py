import sqlite3
from sqlite3.dbapi2 import connect
from flask_restful import Resource, reqparse
from flask_jwt import jwt_required

#Todos los resource debenn ser clases
class Item(Resource):
 ##Inicio de Código de clase reutilizable por otros metodos de la clase (post y put)#####
    parser = reqparse.RequestParser() #habilitado con el modulo request para modificar tan solo una parte del payload 
    # y al cual se le puede indicar ciertos parámetros adicionales (que sea float , info, con valor ..)
    # TAMBIÉN le estamos indicando que el el único argumente esperado es 'price'
    
    parser.add_argument('price',
        type=float,
        required=True,
        help="This field cannot be left blank!"            
    )
##Fin de Código de clase reutilizable por otros metodos de la clase (post y put)#####
    @jwt_required() # con este requiered , el siguiente método debe contener en el header de postman 
    #una key de tipo 'Authorization'con valor JWT seguida de la cadena access_token devuelta por /auth previamente.
    def get(self, name): #con el nombre del item
        """
        # for item in items:
        #     if item['name'] == name:
        #         return item
        item = next(filter(lambda x: x['name'] == name, items), None) 
        #la anterior linea sustituye al bucle for: busca mediante next, filter y lambda, la primera coincidencia. 
        # Con none evitamos  una salida de error en el caso de que la lista se ecuentre vacía.
        
        return {'item': item}, 200 if item else 404 # El error que debe devolver cuando no encuentra algo es 404
        """
        item = self.find_by_name(name)
        if item:
            return item
        return {'message': 'Item not found'},404
    @classmethod
    def find_by_name(cls, name):
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        query = "SELECT * FROM items WHERE name=?"
        result = cursor.execute(query,(name,))
        row = result.fetchone()
        connection.close()
        if row:
            return {'item': row[0], 'price': row[1]}

    def post(self, name):
        #item = {'name': name, 'price': 12.00}
        """
        if next(filter(lambda x: x['name'] == name, items), None): # si no es None 
        """ 
        if self.find_by_name(name):    
            return {'message': "An item with name '{}' already exists'".format(name)}, 400 #Este código es para un bad request
        
        #importante importar request from flask para que funcione la request.get_json: con 'force=True' le indicamos que no necesitamos el context-type
        #y que lo formatearemos incluso aunque no tenga application/json
        #con 'silent=True' no da ningún error
            # data = request.get_json(force=True) 
            # data = request.get_json(silent=True) 
        data = Item.parser.parse_args() # hace una llamada al código definido al principio de la clase
        item = {'name': name,'price': data['price']}
        try: 
            self.insert(item) #Donde el item es el diccionario de la línea anterior
        except:
            return {"message" : "An error ocurred inserting the item."}, 500 #Devolverá un error de tipo "internal server"  
        #items.append(item) código obsoleto al utilizar bb.dd.
        return item, 201 #El código a devolver cuando algo se crea correctamente e 201
    @classmethod
    def insert(cls, item):
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        query = "INSERT INTO items VALUES (?, ?)"
        cursor.execute(query, (item['name'], item['price']))
        connection.commit()
        connection.close()

    def delete (self,name):
        
        """ global items
        items = list(filter(lambda x: x['name'] != name, items))
        #ya que se está dirigiendo a una misma variable con 'global items' le inidicamos que modifique la lista global items
        """   
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        query = "DELETE FROM items  WHERE name=?"
        cursor.execute(query, (name,))
        connection.commit()
        connection.close()
        return {'message': 'item deleted'}
    
    
    def put(self, name):  #crea o actualiza un determinado item. No importa cuantas veces se llame que siempre actualizará el mismo item.
        data = Item.parser.parse_args() # hace una llamada al código definido al principio de la clase
        # item = next(filter(lambda x: x['name'] == name, items), None) #obsoleto al realizar la consulta sobre bb.dd.
        item = self.find_by_name(name)
        update_item = {'name': name, 'price': data['price']}
        if item is None:
            try:
                self.insert(update_item)
            except: 
                return {"message" : "An error ocurred inserting the item."}, 500 #Devolverá un error de tipo "internal server"                  
        else:
            try:
                self.update(update_item) #si existe el item ,actualiza sus datos.
            except: 
                return {"message" : "An error ocurred updating the item."}, 500 #Devolverá un error de tipo "internal server"                  
            #obtenidos en item con la función lambda        
        return update_item 
    
    @classmethod
    def update(cls, item):
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        query = "UPDATE items SET price=? WHERE name=?"
        cursor.execute(query, (item['price'], item['name']))
        connection.commit()
        connection.close()
        return {'message': 'item deleted'}

class ItemList(Resource):
    def get(self):
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        query = "SELECT * FROM items"
        result = cursor.execute(query)
        items = []
        for row in result:
            items.append({'name': row[0], 'price': row[1]})
        connection.close()
        return {'items': items} #devolveremos la lista que hemos ido reyenando con la consulta a bb.dd.

