from flask import Flask
from flask.json import jsonify
from flask_restful import  Api
from flask_jwt import JWT
# Flask-JWT srive para controlar la autenticación. pip istall Flask-JWT y se mostrará como PyJWT
# también se importa el decorator @jwt_requiered de tal manera que es necesario autenticar cuando se
# antepone a la llamada a algún método.
# el módulo reqparse sirve para en la request modificar solo una parte del payload(dato)
from security import authenticate, identity
# Resource , se utilizar por ejemplo en el caso de artículos, pianos , será un resoure
# Los resource son normalmente mapeados a BB.DD. por lo que es algo a tener en cuenta.
from user import UserRegister
from item import Item, ItemList
app = Flask(__name__)
app.secret_key = 'password'
api = Api(app) #Permite de una manera muy fácil añadir resources a nuestra app. 
jwt = JWT(app,authenticate, identity) 
# JWT crea un nuevo endpoint > /auth, cuando llamamos a auth se envia un user y password
# el endpoint /auth de postman se ha creado un header tipo content-type con value application/json y en 
# body se introducido coomo valor
# raw:  
    # {
    # "username": "bob",
    # "password": "asdf"
    # }
#items = [] # No hace falta desde que en se accede a los datos desde database
# IMPORTANTE: CON FLASK RESTFULL YA NO ES NECESARIO UTILIZAR JSONIFY PARA DEVOLVER EL JSON YA QUE DEVUELVE 
# EL DICCIONARIO PYTHON POR SI MISMO





api.add_resource(Item, '/item/<string:name>') #el modo de acceder a los endpoing similar a los decoradores @app.route('/store')
api.add_resource(ItemList, '/items')
api.add_resource(UserRegister, '/register')

#SOBRE IMPORT:Para evitar que se ejecute app.run al importar a otro .py , se especifica que __name__=='__main__' 
#de tal manera que la se ejecutar solo cuando hagamos un python app.py y no en otro caso.

if __name__ == '__main__':
    app.run(port=5000, debug=True) #con el parámetro debug podemos depurar los errores mostrados.

