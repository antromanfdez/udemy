import sqlite3
from sqlite3.dbapi2 import Cursor
connection = sqlite3.connect('data.db')
cursor = connection.cursor()
create_table = "CREATE TABLE if NOT EXISTS users (id INTEGER PRIMARY KEY, username text, password text)"
#Para incrementar el id se utiliza INTEGER PRIMARY KEY en vez de id INT
cursor.execute(create_table)
# user = (1, 'bob', 'asdf')
# insert_query = "INSERT INTO users VALUES (?,?,?)"
# cursor.execute(insert_query, user)

create_table = "CREATE TABLE if NOT EXISTS items (name text, price real)"
cursor.execute(create_table)
# cursor.execute("INSERT INTO items VALUES ('test',10.99)")
connection.commit()
connection.close()
