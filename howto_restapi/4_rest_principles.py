"""
- REST API responde con RESOURCES similar a objetos. 

- Introduce el concepto de STATELESS que significa que un request no puede depender de otro request
- El servidor solo cononoce sobre la la request actual y no sabe nada de la request previa. 

    Ej. Si creo un item con POST /item/chair , el servidor no sabe que item nuevo existe.
    El request GET /item/chair irá a la bb.dd. y chequeará si l item consultado se encuentra.

    Ej. Un usuario hace login en la web, el web server no sabe si el usuario se encuentra logado ya 
        que no recuerda los estados. 

        LA APLICACION WEB DEBE ENVIAR LA SUFICIENTE INFORMACIÓN PARA IDENTIFICAR AL USUARIO EN CADA REQUEST, 
        EN CASO CONTRARIO NO SE ASOCIARÁ LA REQUEST AL USUARIO. 

"""