""" 
    lambda es una función especial sin nombre que solo devuelve un valor. Para el ejemplo de ejercicios anteriores se podría hacer

    def add(x, y): 
    
        return x + y 
    
    print(add(3,4))  


"""

# <lambda> <parameters>:<return value>
def double(x):
    return x * 2

sequence = [1, 3 , 5, 9]
print("método de list comprehensions")
doubled = [double(x) for x in sequence] 
print(doubled)
print("este método a través de map es comunmente usado en otro lenguajes como javascrit")
doubled = list(map(double,sequence)) 
print("método lambda")
doubled = list(map(lambda x: x * 2,sequence)) 

print(doubled)

""" 
    Como recomendación, mejor utilizar funciones normales a una función lambda ya que puede resultar confuso según 
    va creciendo el código. 
"""