def add(x, y):
    result = x + y 
    print (result)

add(5, 3)
#no se puede pasar un argumento a una funcion en la que no exitan ese parámetro definido en la función o al contrario.
#Mostrará error en tal caso.

    ##add() #TypeError: add() missing 2 required positional arguments: 'x' and 'y'

# Para saber a qué parámetro se el asigna un determinado argumento , se le puede especificar en la llamada a la función.
def say_hello(name, surname):
    print (f"Hola {name}, {surname}")

say_hello(surname="Roman", name = "Antonio")