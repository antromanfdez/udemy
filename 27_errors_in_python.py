print ("===================Programa 1===============================")
# Error son utilizados para controlar el flujo de datos y línea dode sucede el error. 
def divide(dividend, divisor):
    if divisor == 0:
        # print ("Divisor cannot be 0")
        # return 
        raise ZeroDivisionError ("Divisor cannot be 0") #Sustituye a las dos lineas anteriores.
    return dividend / divisor
# divide(15, 0)
# grades = [78, 99, 85, 100]
grades = []
f="l"
print ("Welcome to the average grade program")
try: 
    # average = divide(sum(grades), len(grades))
    average = divide(sum(grades), 0)
#Se pueden generar tantas excepciones de error como querámos.     
except ZeroDivisionError as error: #caputra el string de raise en la variable que querámos p.ej."error" 
    print(error)
    print("Atención: The are no grades yet in your list")

else: # si la ejecución se realiza satifactoriamente en en try se ejecutaría este else
    print(f"The avergae grade is {average}")
finally: # sirve para ejecutar siempre , independientemente de si hay error o no 
    print("Programa finalizado.")

print ("===================Programa 2===============================")
students = [
    {"name": "Bob", "grades": [4, 9, 4]},
    {"name": "Bob", "grades": [0]},
    {"name": "Bob", "grades": [4, 9, 4]},
]

try:
    for student in students:
        name = student["name"]
        grades = student["grades"]
        average = divide(sum(grades), len(grades))
        print(f"{name} promedio: {average}.")       
except ZeroDivisionError:
    print(f"ERROR: {name} no tiene notas")
else:
    print("Se han calculado todas las notas")
finally:
    print("Calculo finalizado. Ejecutado incondicionalmente.")
    



