# No usar parámetros por defececto que sean MUTABLES: Ej. lista de parámetros

# Los valores inmutabes son : enteros , cadenas ,flotante, booleano, es decir , para el siguiente ejemplo 
# al ser el mismo valor las variables son inmutables al ser tratado por python como un objeto

# a=4
# b=4
# print(id(a)) -->140712302568176
# print(id(b)) -->140712302568176

from typing import List
print ("#######################ejemplo mal")
class Student: 
    def __init__(self, name: str, grades: List[int] = []): #Esto no se debe de hacer 
        #ya que se inicia grades cuando la clase se ha definido y no cuando es llamado por lo que siempre tendrá el mismo valor

        self.name = name
        self.grades = grades
    
    def take_exam(self, result):
        self.grades.append(result)

bob = Student("Bob")
bob.take_exam(90)
peri = Student("Perico")
print(bob.grades)
print(peri.grades)

print ("#######################ejemplo bien")
from typing import List, Optional 
class Student: 
    def __init__(self, name: str, grades: Optional[List[int]] = None): 
        #forma correcta con Optional, donde se le indica que ese parámetro es opcional. 
        self.name = name
        self.grades = grades or []
     
    def take_exam(self, result):
        self.grades.append(result)

bob = Student("Bob")
bob.take_exam(90)
peri = Student("Perico")
print(bob.grades)
print(peri.grades)
