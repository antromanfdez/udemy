#while loops 
""" number = 7
while True:
    user_input = input("Desea jugar? (Y/n) ")
    if user_input == "n":
        break
    user_number = int(input("Introduzca un número: "))
    if user_number == number:
        print("Es es mismo")
    elif abs(number - user_number) == 1: # El número absoluto sin signo. 
        print ("Te quedate a 1 de diferencia")
    else:
        print("Lo siento, no has acertado") """

#for loops
amigos = ["Bob","Perico","Luis"]
for amigo in amigos:
    print(f"{amigo} es mi amigo")

#l a funcion 'range' crea una lista de los valores que se especifiquen

for i in range(20):
    print(i)