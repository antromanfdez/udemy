# La diferencia entre 'class inheritance' y 'class_composition' es que la primera, una clase pertenece a otra,

# es decir, una clase impresora pertenece a la clase dispositivo mientra que con 'class composition' 

# una clase tiene muchas clases , por ejemplo, una clase librería, tiene un montón de clases del tipo 'libro'

class BookShelf: 
    def __init__(self, *books):
        self.books = books
    def __str__(self):
        return f"BookShelf with {len(self.books)} books"

class Book:
    def __init__(self, name):
        self.name = name
    
    def __str__(self) -> str:
        return f"Book {self.name}"

book = Book("Harry Potter")
book2 = Book("La sombre del viento")
shelf = BookShelf(book, book2)
print(shelf)