# # x= 15
# # price = 9.99
# # discount = 0.2
# # result = price * (1 - discount)
# # print (result)

""" # # name = "Pedro"
# # print (name*2)

# # a = 25
# # b = 14
# # b = a
# # print (a)
# # b = 19
# print (b) """
#-- variables 1
""" name = "Bob" 
greeting = f"Hello, {name}"
print(greeting)
name = "Perico"
greeting = f"Hello, {name}"
print(greeting)  
 """#-- variables 1
#-- variables 2
""" name = "Rodri"
greeting = "Hello, {}"
var1 = greeting.format("Rolf")
var2 = greeting.format("Perico")
var3 = greeting.format("Manué")
print(var1)
print(var2)
print(var3) """
#-- variables 2

#-- variables 3
longer_phrase = "Hello, {}, Today is {}"
formatted = longer_phrase.format("Rodri","Perico")
print(formatted)
#-- variables 3

