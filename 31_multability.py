
a = []
b = a
print(id(a))
print(id(b))


# En python todo es mutable a menos que se especifique lo contrario. a y b tiene el mismo ID, pero si modifico
# alguno de ellos el valor cambiar. 
# En el caso de las tuplas SI SON INMUTABLES. 
a = []
b = []
# En python [] es la instrucción para crear  una empty list que si difiere de la de otra variable
print(id(a))
print(id(b))
a=4
b=4
print(id(a))
print(id(b))