# class_inheritance
# permite a una clase tomar algunos métodos y clases de otro método.

class Device:
    def __init__(self, name, connected_by):
        self.name = name
        self.connected_by = connected_by
        self.connected = True
    
    def __str__(self):
        return f"Device {self.name!r} ({self.connected_by})" # con !r se entrecompilla el valor name
    
    def disconnect(self):
        self.connected = False
        print ("Disconnected")

# printer = Device("Epson", "USB")
# print(printer)
# printer.disconnect()

# Crreamos la clase printer que tomará los métodos de device
class Printer (Device):
    def __init__(self,name,connected_by,capacity):
        super().__init__(name, connected_by) 
        #con super le indicamos que utilice init del parent class Device : 
            # self.name = name
            # self.connected_by = connected_by
            # self.connected = True
        self.capacity = capacity
        self.remaining_pages = capacity 

    def __str__(self):
        return f"{super().__str__()}({self.remaining_pages} pages remaining)"
    
    def print(self,pages):
        if not self.connected:
            print("Your printer is not connected!")
            return
        self.remaining_pages -= pages
        
printer = Printer ("Epson", "USB", 400)
print(printer)
printer.print(20)
print(printer)
printer.disconnect() # python intentará buscar este método en Printer, al no enconrarlo lo buscará en su parent class 'Device'
printer.print(20)
