print("Ejemplo unpacking arguments 1")
""" 
    la función  tendrá una colección de n argumentos cuando la función es llamada
""" 
def multiply(*args):
    total = 1
    for arg in args:
        total = total * arg
    return total

print(multiply(1, 2, 3, 4))
print(multiply(1, 2, 3, 4, 9))
print("Ejemplo unpacking arguments 2")
""" 
    con el siguiente ejemplo se le indica con '*nums' que de la lista se debe asignar un valor a cada uno de los argumentos
    de la función. En el caso de no poner nada se pasaría la lista completa al primero argumento, en esta caso a la variable 'x'
    LO SIGUIENTE FUNCIONA CONOCIENDO EL NÚMERO DE ARGUMENTEOS.
"""
def add(x, y):
    return x + y
nums = [2, 3]
print(add(*nums)) 
print("Ejemplo unpacking arguments 3")
"""     
        Para poder pasar una clave valor a una función sin necesidad de hacer lo mostrador en la linea:
        'print(add(x=nums2["x"], y=nums2["y"])) ' se utiliza el argumento **args.
        
 """
def add2(x, y):
    return x + y
nums2 = {"x":15, "y": 20}
#print(add2(*nums2)) 
print(add2(x=nums2["x"], y=nums2["y"])) 
print(add2(**nums2)) # haría lo mismo que la línea anterior.
print("Ejemplo unpacking arguments 4")
"""
    Se puede pasar una colección de argumentos con *args y un el nombre de una variable. 
    En el siguiente ejemplo se muestra un ejemplo con n argumento y un nombre de una variable con el 
    se le indica la operacíon a realizar. 
"""
def apply(*args, operator):
    if operator == "*":
        return multiply(*args)
    else:
        sum(*args)

print(apply(2,2,operator="*"))






