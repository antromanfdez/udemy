#Ejemplo 1
numbers = [1, 3, 5]
doubled = [x * 2 for x in numbers] #facilidad de python para crear iteraciones en una linea creando otra lista "list comprenhensions"
print (doubled)

#Ejemplo 1

friends = ["Lola", "Paco", "Pablo", "Luis"]
start_s = []
for f in friends:
    if f.startswith("P"):
        	start_s.append(f)
print(start_s)

# el loop anterior se podía resumir con "list comprenhensions con la siguiente linea"
friends2 = ["Lola", "Paco", "Pablo", "Luis"]
start_s2 = [f for f in friends if f.startswith("L")]
print(start_s2)