# Sirve como aclaración a los tipos de datos que se va a utilizar además de advertir si se
# introducen tipos erróneos¡. En el el ejemplo se pasa a la función
# una lista y se retorna un float
#       def list_avg(sequence: list) -> float: 
from typing import List
def list_avg(sequence: list) -> float:
    return sum(sequence) / len(sequence)


list = [0,1,2,3,4,5]
print (list_avg(list))


class Book:
    TYPES = ("hardcover","paperback")
    def __init__(self,name: str, book_type: str, weight: int):
        self.name = name
        self.book_type = book_type
        self.weight = weight
    def __repr__(self) -> str: 
        return f"<Book: {self.name}, {self.book_type}, weighing {self.weight}g>"
    
    @classmethod
    def hardcover(cls, name: str, page_weight: int) -> "Book":
        #Book debe estar entrecomillado si es el mismo objet de la clase en la qu se encuentra
        #  e indica que devuele el mismo objeto de la clase Book

        return cls(name, cls.TYPES[0],page_weight + 100)

    @classmethod
    def paperback(cls, name, page_weight) -> "Book":
        return cls(name, cls.TYPES[1],page_weight)
    

book = Book("Harry Poter", "paperback", 1500)
print(book)
bookcaro = Book.hardcover("El Quijote", 2000)
bookbarato = Book.paperback("El Quijote", 2000)
print(f"Libro barato: {bookbarato}")
print(f"Libro caro: {bookcaro}")