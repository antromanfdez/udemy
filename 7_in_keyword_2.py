#comprobar si un elemento se encuentra dentro de otro. 

l = ["Bob","Perico","Luis"]
print("Bob" in l)
movies = {"Matrix", "Lebowsky", "Green Book"}
user_movie = input("Introduzca la pelicula que has visto: ")
if user_movie in movies:
    print(f"Yo también he visto {user_movie}")
else: 
    print(f"No he visto {user_movie}")

print("####################")

number = 7
user_input = input("Introduzca 'y' si desea jugar: ").lower()
if user_input == "y":
    user_number = int(input("Introduzca un número: "))
    if user_number == number:
        print("Es es mismo")
    elif abs(number - user_number) == 1: # El número absoluto sin signo. 
        print ("Te quedate a 1 de diferencia")
    else:
        print("Lo siento, no has acertado")
