#se puede jugar con set con la propiedad "diference" para buscar diferencias. 
""" friends = {"Bob", "Rolf", "Donovan"}
abroad = {"Bob", "Rolf"}
local_friends = friends.difference(abroad) 
print(local_friends)
 """
#Con la propiedad "union" pruebas agrugar todos aquellos elementos diferentes en otro set

""" friends = {"Bob", "Rolf", "Donovan"}
abroad = {"Bob", "Rolf","Nacho"}
local_friends = friends.union(abroad) 
print(local_friends) """
#Con la propiedad "intersection" buscar que elementos se encuentra a la vez en diferentes set.

""" art  =  {"Bob", "Rolf", "Donovan"}
science = {"Bob", "Rolf","Nacho"}
both = art.intersection (science)
print(both) """

# Modify set2 so that set1.intersection(set2) returns {5, 77, 9, 12}
set1 = {14, 5, 9, 31, 12, 77, 67, 8}
set2 = {5}
set2 = {5, 77, 9, 12}
both = set1.intersection(set2)
print(both)

my_list = [20, 60, 20]
my_tuple = (14,)
set1 = {5,77,9,12}
set2 = {5,77,9,12,21}
my_list = ["20", "60", "20"]
my_tuple = ("1")
set2=set1.intersection(set2)
print(set2)

