
class Student: 
    # Las clases comienza con mayúsculas para diferenciarlas de las variables y funciones.
    # Una función dentro de una clase es un método.
    def __init__(self): #Este método sirve para iniciar la clase.
        self.name ="Rolf"
        self.grades = (90, 90, 93, 12, 12)
    def average(self):
        return sum(self.grades) / len(self.grades)
        """ 
            Este método dentro de la clase cojerá self (convención) como parámetro que será el parámetro 
            que se ha creado inicialmente.
         """
"""
    Se llama a una clase que tiene un método init donde se la pasa la información que queramos. 
    Esta calse se pasa al objeto student y que podrá llamarse a continuación con student.name, student.grade etc.
 """
student = Student() 
print(student.name)
print(student.grades)
""" 
    En la siguiente linea se hace una llamada al método Student.average() y se le pasa como parámetro el propio objeto 
    que previamennte he inicializado con la misma clase. -> student = Student() 

    Se podría hacer con los dos siguientes comandos. Con el segundo se llama al objeto y simplifica la llamada al 
    métod. 
"""
print(Student.average(student)) 
print(student.average()) 
""" 
    En la definición de la clase se pueden definiar más parámetros además de los propiamene definidos en la clase (self.X) 
    
 """
class Student2: 
    # Las clases comienza con mayúsculas para diferenciarlas de las variables y funciones.
    # Una función dentro de una clase es un método.
    def __init__(self, name,grades): #Este método sirve para iniciar la clase.
        #En la siguiente linea se observa el empleo del parametro name que será pasado en la creación del objeto
        self.name = name 
        self.grades = grades

    def average(self):
        return sum(self.grades) / len(self.grades)
student2 = Student2("Pablo", (12,45,98))
print(student2.name)
print(student2.grades)
print(student2.average())

