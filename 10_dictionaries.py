# Con diccionarios se asocian key to value
""" friends_age = {"Rolf": 24, "Alberto": 41}
print(friends_age["Alberto"])
friends_age["Alberto"] = 31 #Modificar un valor del diccionario
print(friends_age["Alberto"])
print(friends_age) """

#lista de diccionarios
""" friends2 = [
    {"name": "Rolf", "age": 23},
    {"name": "Laura", "age": 42},
    {"name": "Sonia", "age": 52},
]
print(friends2[1]["name"]) """

#acceder a key
student_attendance = {"Maria" : 96, "Jose" : 45, "Luis" :28}
for student in student_attendance:
    print(student)
#conociendo la key , acceder al valor
for student in student_attendance:
    print(f"{student}: {student_attendance[student]}")
#LA MANERA MÁS EFICIENTE DE ITERAR EN DICCIONARIO PARA EXTAER LOS VALORES ES CON ITEMS.(). PERMITE EXTRAER LA CLAVE Y VALOR 
print ("Utilizando items")
for student, attendance in student_attendance.items():
    print(f"{student}: {attendance}")
print("Contiene la key Maria")
if "Maria" in student_attendance:
    print(f"Maria: {student_attendance['Maria']}")

#acceder a los valores del diccionario para operar con ellos. 
attendance_values=student_attendance.values()
print(sum(attendance_values)/len(attendance_values))
