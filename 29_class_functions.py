from operator import itemgetter
print("##################################################Ejer1 ")
def divide(dividend,divisor):
    if divisor == 0:
        raise ZeroDivisionError("Divisor cannot be 0")
    return dividend / divisor

def calculate (*values, operator):
    return operator(*values)
result = calculate (4, 2, operator=divide) #para pasar el parámetro de la funcion se debe realizar con "operator=divide"
print(result)

print("##################################################Ejer2 ")

def search(sequence, expected, finder):
    for elem in sequence:
        if finder(elem) == expected:
            return elem
    raise RuntimeError(f"Could not find an elelment with {expected}")

friends = [
    {"name": "Rolf", "age": 41},
    {"name": "Adam Wool", "age": 21},
    {"name": "Julio Cortazar", "age": 61},
]

def get_friends_name(friend):
    return friend["name"]

print(search(friends, "Rolf", get_friends_name))
print(search(friends, "Rolf", lambda friend: friend["name"])) #alternativa a crear la funcion con lamda
print(search(friends, "Rolf", itemgetter("name"))) #alternativa a las anteriores utilizando itemgetter