#las variables se pueden asignar por posición pudiendo hacer los siguiente:
t = 4,3
x, y = t
print(x,y)
# El uso del signo underscore "_" indica a python que ignore una variable
names = ("Bob",24,"Carpintero")
name, _, profesion = names
# para coleccionar elementos se puede utilizar el asterisco "*" , el siguiente ejemplo , a partir del segundo elemento 
#se almacenará la lista en la variables "tail"
head , *tail = [1, 2, 3, 4, 5]
print(*tail)