class TooManyPagesReadError(ValueError): 
    # Esta clase viene a ser una copia de lo que haría ValueError pero con otro name
    pass

class Book:
    def __init__(self, name: str, pages_count: int): 
        self.name = name
        self.pages_count = pages_count
        self.pages_read = 0            
        if not type(pages_count) is int:
            raise "El segundo parámetro debe ser un entero."
        
    def __repr__(self):
        return (
            f"<Book {self.name}, read {self.pages_read} pages out of {self.pages_count}>"
        )
    def read(self,pages):
        if not type(pages) is int:
            raise "El parámetro introducido debe ser entero"
        if self.pages_read + pages > self.pages_count:
            raise TooManyPagesReadError(
                f"You tried to read {self.pages_read + pages} pages, but this book only as {self.pages_count}" 
            )
        self.pages_read += pages
        print (f"You have now read {self.pages_read} pages out of {self.pages_count})")
        

python101 = Book("Python 101", 120)
python101.read()
# python101.read(10)
# python101.read(80)
python101 = Book("Python 101", 12)
### Para hacerlo más userfriendly se puede hacer con try y except 
# try: 
#     python101 = Book("Python 101", 50)
#     python101.read(10)
#     python101.read(10)
#     python101.read(2)
#     python101.read(80)
#     python101.read(120)
# except TooManyPagesReadError as e:
#     print(e)