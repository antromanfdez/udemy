l = ["Bob","Perico","Luis"] #el valor es mutable y tiene un orden 
t = ("Bob","Perico","Luis") #el valor es permanente y tiene un orden. No se pueden añadir a eliminar elementos.
s = {"Bob","Perico","Luis"} #no puedes tener elementos duplicados y no tiene un orden 
""" print(l[0]) """
l[0] = "Smith" #Esto no funciona para tuplas
l.append("Pablo")
print(l)
l.remove("Perico")
print(l)
print(t[0])
s.add("Smith") #Añade un elmeneto a sets
print("Imprime Set",s)
print(s)
print(s)
