class ClassTest:
    
    
    def instance_method(self): #TIPO INSTANCE METHOD
        """ 
        Todas las clases que utilicen como primer parámetro "self" son conocidos como "instance method"
        "instance method" necesitan el objeto para ser llamado:
        ESTE MÉTODO ES EL MÁS COMÚN YA QUE SE UTILIZAR PARA MODIFICAR LOS DATOS DEL OBJETO
        CREADO, MODIFICAR LOS DATOS DENTRO DEL OBJETO.
        """

        print(f"Called instance_method of: {self}")
    @classmethod
    def class_method(cls): 
        """ 
        necesita un parámetro que será como resultado la propia definición de la clase 'ClassTest'
        ESTE MÉTODO SE UTILIZA COMO FACTORY. Neceista el parámetro cls viene a indicar la clase que se le pasa

        """
        print(f"Called class_method of: {cls}")
    @staticmethod
    def static_method(): #Este método no obtiene nada.
        print ("Called static")

test = ClassTest()
test.instance_method()
ClassTest.instance_method(test)

ClassTest.class_method() #por defecto python pasará como argumento la propia clase 'ClassTest'  a través del parámetro 'cls'

ClassTest.static_method()