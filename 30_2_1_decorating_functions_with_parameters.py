import functools
user = {"username": "Jose", "access_level": "admin"}

def make_secure(func):
    @functools.wraps(func) #este decorador sirve para que secure_function sea un wrapper de make_secure
    #manteniiendo el nombre de la función que se le pase 
    # print(get_admin_password.__name__) ---> get_admin_password
    # sin @functools.wraps(func) ---> secure_function
    def secure_function(*args, **xargs):
        if user["access_level"] == "admin":
            return func(*args, **xargs) #retorna la función original que se le pasó por parámetros con los 
            #parámetros que se les pase
        else:
            return f"No admin permissions for {user['username']}"
    return secure_function

@make_secure 
# la linea anterior antes de la función get_admin_password() hara que la función se cree y se pase directamente
#al decorador  cuando sea llamado evitando que se puede hacer por ejemplo print(get_admin_password()) de manera 
# externa pudiendo mostrar el código 

def get_password(panel):
    if panel == "admin":
        return "1234"
    elif panel == "billing":
        return "super_secure_password"

# get_admin_password = make_secure(get_admin_password) # esta linea es sustituida por @make_secure
print(get_password("billing"))

print(get_password.__name__)
