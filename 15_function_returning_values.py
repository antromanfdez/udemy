""" 
def add(x, y): 
    print(x + y)
    return

print(add(2, 4)) """

""" Por defecto, una función devuelve 'None' cuando no se le indica el parámetro seguido del valor 'return'. En la función anterior devuelve:

    6
    None 
"""
def add(x, y): 
    
    return x + y 
    """ 
    puede haber más de un return en una misma función por ejemplo a través de una condición , se aconseja que el valor 
    devuelto sea siempre el mismo.  """
    
print(add(3,4))
