#Con este ejemplo de decorators se consigue una capa más de abstracción añadiendo una función adicional anidada
# y permitiendo pasar a través de make_secure el parámetro a comparar
import functools
user = {"username": "Jose", "access_level": "admin"}

def make_secure(access_level):
    def decorator(func):
        @functools.wraps(func) 
        def secure_function(*args, **xargs):
            if user["access_level"] == access_level:
                return func(*args, **xargs) 
            else:
                return f"No {access_level} permissions for {user['username']}"
        return secure_function
    return decorator

@make_secure("perico") 
def get_admin_password():
    return "admin: 1234"

@make_secure("perico")
def get_dashboard_password():
    return "user: user_password"

print(get_admin_password())
print(get_dashboard_password())
