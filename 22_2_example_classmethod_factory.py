
#Ejemplo de metodos de clase utilizando la técnica de factory que devolverá objetos en función de un tipo u otro.

 
class Book:
    TYPES = ("hardcover","paperback")
    def __init__(self,name, book_type, weight):
        self.name = name
        self.book_type = book_type
        self.weight = weight
    def __repr__(self): 
        return f"<Book: {self.name}, {self.book_type}, weighing {self.weight}g>"
    
    @classmethod
    def hardcover(cls, name, page_weight):
        return cls(name, cls.TYPES[0],page_weight + 100)

    @classmethod
    def paperback(cls, name, page_weight):
        return cls(name, cls.TYPES[1],page_weight)
    

book = Book("Harry Poter", "paperback", 1500)
print(book)
bookcaro = Book.hardcover("El Quijote", 2000)
bookbarato = Book.paperback("El Quijote", 2000)
print(f"Libro barato: {bookbarato}")
print(f"Libro caro: {bookcaro}")
     