""" 
    Método correcto para no tener que recorrer el diccionario utilizando un mapeo. 
    En este caso mapeo la lista obteniendo el usuario como clave de cada una de las tuplas con 
    el fin de poder acceder al valor 'password' de un determinado usuario. 
 """
users = [
    (0, "Julio", "sdfasdf"),
    (1, "Luis", "sdfaasdfsfd234f"),
    (2, "Ulises", "sdsdff"),
]

username_mapping = {user[1]: user for user in users}

username_input = input("Enter your usernanme: ")
password_input = input("Enter your password: ")
_, username, password = username_mapping[username_input]
if password_input == password:
    print("Your password is correct")
else:
    print("Error")






